package com.spring.boot.example.SpringWebServiceExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebServiceExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebServiceExampleApplication.class, args);
	}
}
